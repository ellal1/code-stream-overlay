import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import {
  HashRouter as Router,
  Switch,
  Link,
  Route,
  useLocation
} from 'react-router-dom'
import store from './store' // Custom redux store
import './index.css'

// **** Components ****
import Hour from '../src/Hour/Hour'
import Guideline from '../src/Guideline/Guideline'
import PresenceGrid from '../src/PresenceGrid/PresenceGrid'
import Summary from '../src/Summary/Summary'
import Survey from '../src/Survey/Survey'
import Question from '../src/Question/Question'

const Home = () => {
  const currentPage = useLocation().pathname
  return (
    <Provider store={store}> {/* Allows to use the store from the entire app */}
      <div className='menu'>
        <nav className='home-nav'>
          <ul className='list'>
            <li className={currentPage === '/call' ? 'list__element list__element--active' : 'list__element'}>
              <Link to='/call'>Call</Link>
            </li>
            <li className={currentPage === '/hour' ? 'list__element list__element--active' : 'list__element'}>
              <Link to='/hour'>Hour</Link>
            </li>
            <li className={currentPage === '/guideline' ? 'list__element list__element--active' : 'list__element'}>
              <Link to='/guideline'>Guideline</Link>
            </li>
            <li className={currentPage === '/summary' ? 'list__element list__element--active' : 'list__element'}>
              <Link to='/summary'>Summary</Link>
            </li>
            <li className={currentPage === '/survey' ? 'list__element list__element--active' : 'list__element'}>
              <Link to='/survey'>Survey</Link>
            </li>
            <li className={currentPage === '/questions' ? 'list__element list__element--active' : 'list__element'}>
              <Link to='/questions'>Questions</Link>
            </li>
          </ul>
        </nav>
      </div>
      <Switch>
        <Route path='/call'>
          <PresenceGrid />
        </Route>
        <Route path='/hour'>
          <Hour />
        </Route>
        <Route path='/guideline'>
          <Guideline />
        </Route>
        <Route path='/summary'>
          <Summary />
        </Route>
        <Route path='/survey'>
          <Survey />
        </Route>
        <Route path='/questions'>
          <Question />
        </Route>
      </Switch>
    </Provider>
  )
}

ReactDOM.render(
  <Router>
    <Provider store={store}> {/* Allows to use the store from the entire app */}
      <Home />
    </Provider>
  </Router>
  , document.getElementById('root')
)
