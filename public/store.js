/*
   This file manages global state of the application
   using a redux.js store
 */
import { createStore, applyMiddleware } from 'redux'
import { processChatMsg } from './store_actions'
import fetch from 'node-fetch'
import thunkMiddleware from 'redux-thunk'

// **** INITIAL STATE OF THE STORE ****
const initialState = {
  call: {
    isDisplayed: false,
    studentsList: []
  },
  question: {
    queue: [],
    currentIndex: null, // index of current question in queue
    isDisplayed: false
  },
  clock: {
    isDisplayed: true,
    alarmHour: null // hour of the next alarm
  },
  guidelines: {
    isDisplayed: true,
    time: {}, // time to complite the task
    guideline: null
  },
  summary: {
    isDisplayed: false,
    summaryElementList: [] // list of all the elements of the summary
  },
  survey: {
    isDisplayed: false,
    question: null,
    options: []
  }
}

// **** ASYNC ACTION ****
const composedEnhancer = applyMiddleware(thunkMiddleware)

// **** GET LIVE MESSENGER FROM SERVER ****
// Uses custom Nodejs/express SSE server
// run with `node sse-chat-server/sse-chat-server.js`
// (next line is disabled because it actually work)
const eventSource = new EventSource('http://localhost:3000/sse-chat-server') // eslint-disable-line

eventSource.onopen = () => {
  console.log(' >> Connected to sse-chat-server.')
}

eventSource.onmessage = (msg) => {
  console.log('>> New message >> ', msg.data)
  try {
    // Convert ton json and Send message to the store
    const json = JSON.parse(msg.data)
    store.dispatch({ type: 'SET_MSG', message: json })
  } catch (e) {
    console.error('Unable to JSON.parse message : ', msg)
  }
}

// **** REDUX REDUCER ****
// manages different actions on the store
const testReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MSG': // called when a json message arrive from the chat
      return processChatMsg(state, action.message)
    case 'SET_STUDENT_LIST': // called when PresenceGridComponent is created
      return {
        ...state,
        call: {
          ...state.call,
          studentsList: action.payload
        }
      }
    default: // default doesn't change anything
      return state
  }
}

// **** ASYNC JSON FETCH ****
// needed for students list in PresenceGrid component
export async function fetchStudents(dispatch, getState) { // eslint-disable-line
  const response = await fetch('students.json')
    .then((res) => res.json())
  dispatch({ type: 'SET_STUDENT_LIST', payload: response })
}

// **** CREATING THE STORE ****
const store = createStore(testReducer, composedEnhancer)
store.dispatch(fetchStudents)

export default store
