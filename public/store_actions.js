/*
  Manages store advanced actions
*/
import { processCommandCall } from './commands/call_commands'
import { processCommandQuestion } from './commands/questions_commands'
import { processCommandClock } from './commands/clock_commands'
import { processCommandGuidelines } from './commands/guidelines_commands'
import { processCommandSummary } from './commands/summary_commands'
import { processCommandSurvey } from './commands/survey_commands'

// **** DEFINING COMMANDS SYNTAX ****
// most commands have to be called by admin
const commandRegex = /^!!/
const commandCall = /^!!call/
const commandQuestion = /^!!question/
const commandClock = /^!!clock/
const commandGuidelines = /^!!guidelines/
const commandSummary = /^!!summary/
const commandSurvey = /^!!survey/

// **** PROCESS MESSAGE ****
// Process a new chat message to extract commands, emojis ...
export const processChatMsg = (state, message) => {
  if (commandRegex.test(message.content)) {
    console.log('Command entered')
    return processCommand(state, message.content, message.user)
  }
  return state
}

// **** PROCESS COMMAND ****
const processCommand = (state, command, user) => {
  if (commandCall.test(command)) { // CALL
    return processCommandCall(state, command, user)
  } else if (commandQuestion.test(command)) { // QUESTION
    return processCommandQuestion(state, command, user)
  } else if (commandClock.test(command)) { // QUESTION
    return processCommandClock(state, command, user)
  } else if (commandGuidelines.test(command)) { // GUIDELINES
    return processCommandGuidelines(state, command, user)
  } else if (commandSummary.test(command)) { // SUMMARY
    return processCommandSummary(state, command, user)
  } else if (commandSurvey.test(command)) { // SURVEY
    return processCommandSurvey(state, command, user)
  } else {
    console.warn('Unknow command : ', command)
    return state // Nothing changes
  }
}
