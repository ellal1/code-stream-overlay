// Defining Survey commands
const commandSurveyToggle = /^!!survey-toggle/
const commandSurveyNew = /^!!survey-new/
const commandSurveyVote = /^!!survey-vote/

// **** SURVEY COMMANDS MANAGEMENT ****
export const processCommandSurvey = (state, command, user) => {
  if (commandSurveyNew.test(command) && user === 'admin') {
    // !!survey-new creates a new survey
    const newSurvey = command.substring(13).split(' // ')
    console.log('newSurvey : ', newSurvey)
    // !!survey-new question // option1 // option2 // ...
    return {
      ...state,
      survey: {
        question: newSurvey[0],
        options: newSurvey.slice(1).map((option, index) => {
          return {
            id: index,
            choice: option,
            votes: 0
          }
        })
      }
    }
  } else if (commandSurveyVote.test(command) && user === 'viewer') {
    // !!survey-vote votes for one of the survey's options
    const voteIndex = parseInt(command.substring(14, 15))
    console.log(command.substring(14, 15))
    console.log('Survey Vote ' + voteIndex)
    return {
      ...state,
      survey: {
        ...state.survey,
        options: [
          ...state.survey.options.map(
            (option, index) =>
              index === voteIndex ? { ...option, votes: option.votes + 1 } : option
          )
        ]
      }
    }
  } else if (commandSurveyToggle.test(command) && user === 'admin') {
    // !!survey-toggle toggles the component
    console.log('Toggle Survey')
    return {
      ...state,
      survey: {
        ...state.survey,
        isDisplayed: !state.survey.isDisplayed // toggle
      }
    }
  } else {
    console.log('Unknown command : ', command)
    return state
  }
}
