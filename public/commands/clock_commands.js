// **** CALL COMMANDS ****
const commandClockToggle = /^!!clock-toggle/
const commandClockSet = /^!!clock-set/

export const processCommandClock = (state, command, user) => {
  if (commandClockToggle.test(command) && user === 'admin') {
    // !!clock-toggle toggles the clock component
    console.log('Clock toggle')
    return {
      ...state,
      clock: {
        ...state.clock,
        isDisplayed: !state.clock.isDisplayed // toggling
      }
    }
  } else if (commandClockSet.test(command) && user === 'admin') {
    // !!clock-set sets the next alarm
    console.log('Next alarm set to: ' + command.substring(12, 20))
    return {
      ...state,
      clock: {
        ...state.clock,
        alarmHour: command.substring(12, 20)
      }
    }
  } else {
    console.log('Unknown command or wrong user. Nothing changed.')
    return state
  }
}
