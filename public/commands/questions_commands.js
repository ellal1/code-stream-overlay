// Defining Questions commands
const commandQuestionToggle = /^!!question-toggle/
const commandQuestionNext = /^!!question-next/
const commandQuestionPrev = /^!!question-prev/
const commandQuestionNew = /^!!question-new/ // called by viewer

// **** QUESTIONS COMMANDS MANAGEMENT ****
export const processCommandQuestion = (state, command, user) => {
  if (commandQuestionNew.test(command) && user === 'viewer') { // called by viewer
    // !!question-new adds a new question to queue
    const newQuestion = command.substring(14) // remove '!!question-new ' to only get the actul question
    console.log('newQuestion : ', newQuestion)
    return {
      ...state,
      question: {
        ...state.question,
        queue: [...state.question.queue, newQuestion]
      }
    }
  } else if (commandQuestionNext.test(command) && user === 'admin') {
    // !!question-next set the current question to the next one
    console.log('Next question')
    return {
      ...state,
      question: {
        ...state.question,
        currentIndex: state.question.currentIndex + 1 // increment
      }
    }
  } else if (commandQuestionPrev.test(command) && user === 'admin') {
    // !!question-next set the current question to the previous one
    console.log('Prev question')
    return {
      ...state,
      question: {
        ...state.question,
        currentIndex: state.question.currentIndex - 1 // decrement
      }
    }
  } else if (commandQuestionToggle.test(command) && user === 'admin') {
    // !!question-toggle toggles the question component
    console.log('Toggle Question')
    return {
      ...state,
      question: {
        ...state.question,
        isDisplayed: !state.question.isDisplayed
      }
    }
  } else {
    console.warn('Unknown question command : ', command)
    return state // nothing changes
  }
}
