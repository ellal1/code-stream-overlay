// **** CALL COMMANDS ****
const commandCallToggle = /^!!call-toggle/
const commandCallReset = /^!!call-reset/
const commandCallIncrement = /^!!call-present/ // called by viewer

export const processCommandCall = (state, command, user) => {
  if (commandCallToggle.test(command) && user === 'admin') {
    // !!call-toggle toggles the call component
    return {
      ...state,
      call: {
        ...state.call,
        isDisplayed: !state.call.isDisplayed // toggling
      }
    }
  } else if (commandCallReset.test(command) && user === 'admin') {
    /// !!call-reset resets the call counter
    return {
      ...state,
      call: {
        ...state.call,
        studentsList: [] // reset
      }
    }
  } else if (commandCallIncrement.test(command) && user !== 'admin') {
    // !!call-present is called by the viewers and increment present
    console.log()
    return {
      ...state,
      call: {
        ...state.call,
        studentsList:
          // return array with first student that is not present set as present
          state.call.studentsList.map(
            (student, i, a) =>
              student === a.find(s => !s.present) ? { ...student, present: true } : student
          )
      }
    }
  } else {
    console.log('Unknown command or wrong user. Nothing changed.')
    return state
  }
}
