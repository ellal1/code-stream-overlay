// Defining Summary commands
const commandSummaryToggle = /^!!summary-toggle/
const commandSummaryCovered = /^!!summary-covered/
const commandSummaryNewElement = /^!!summary-new/

// **** SUMMARY COMMANDS MANAGEMENT ****
export const processCommandSummary = (state, command, user) => {
  if (commandSummaryNewElement.test(command) && user === 'admin') {
    // !!summary-new adds a new summary element to summary
    const newSummaryElement = command.substring(14) // remove '!!summary-new' to only get the actual element
    console.log('New Summary Element : ', newSummaryElement)
    return {
      ...state,
      summary: {
        ...state.summary,
        summaryElementList: [
          ...state.summary.summaryElementList,
          {
            text: newSummaryElement,
            covered: false
          }
        ]
      }
    }
  } else if (commandSummaryCovered.test(command) && user === 'admin') {
    // !!summary-covered set an element as covered
    console.log('Summary Element covered')
    return {
      ...state,
      summary: {
        ...state.summary,
        summaryElementList: state.summary.summaryElementList.map(
          (ele, index, array) =>
            index === array.findIndex(e => e.covered === false) ? { ...ele, covered: true } : ele
        )
      }
    }
  } else if (commandSummaryToggle.test(command) && user === 'admin') {
    // !!summary-aoggle toggles the questions component
    console.log('Toggle Summary')
    return {
      ...state,
      summary: {
        ...state.summary,
        isDisplayed: !state.summary.isDisplayed
      }
    }
  } else {
    console.log('Unknown command : ', command)
    return state
  }
}
