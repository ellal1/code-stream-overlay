// **** GUIDELINES COMMANDS ****
const commandGuidelinesToggle = /^!!guidelines-toggle/
const commandGuidelinesSet = /^!!guidelines-set/

export const processCommandGuidelines = (state, command, user) => {
  if (commandGuidelinesToggle.test(command) && user === 'admin') {
    // !!guidelines-toggle toggles the guidelines component
    console.log('Guidelines toggle')
    return {
      ...state,
      guidelines: {
        ...state.guidelines,
        isDisplayed: !state.guidelines.isDisplayed // toggling
      }
    }
  } else if (commandGuidelinesSet.test(command) && user === 'admin') {
    // !!guidelines-set sets the next guideline
    const time = command.substring(16, 24).split(':')
    console.log('Next timer and guideline set to: ' + command.substring(25) + time)
    return {
      ...state,
      guidelines: {
        ...state.guidelines,
        time: {
          hours: parseInt(time[0]),
          minutes: parseInt(time[1]),
          seconds: parseInt(time[2])
        },
        guideline: command.substring(25)
      }
    }
  } else {
    console.log('Unknown command or wrong user. Nothing changed.')
    return state
  }
}
