import React from 'react'
import './PresenceGrid.css'
import PresenceCircle from './PresenceCircle'
import { useSelector } from 'react-redux'

const PresenceGrid = () => {
  const students = useSelector(state => state.call.studentsList)
  const display = useSelector(state => state.call.isDisplayed)
  return (
    <div className={display ? 'PresenceGrid' : 'PresenceGrid is-hidden'}>
      {
        students.map(
          (student) => <PresenceCircle key={student.id} present={student.present} />
        )
      }
    </div>
  )
}

export default PresenceGrid
