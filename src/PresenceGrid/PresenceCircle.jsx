import React from 'react'
import './PresenceGrid.css'

const PresenceCircle = (props) => {
  return (
    //  Cirle representing a student. Blue if present, grey if not
    <span className={props.present ? 'PresenceCircle PresenceCircle--present' : 'PresenceCircle'} />
  )
}

export default PresenceCircle
