import React from 'react'
import './Summary.css'
import { useSelector } from 'react-redux'

const Summary = () => {
  const elements = useSelector(state => state.summary.summaryElementList)
  const display = useSelector(state => state.summary.isDisplayed)
  return (
    <div className={display ? 'Summary' : 'Summary is-hidden'}>
      {
        elements.map(element =>
          <span
            key={element.id}
            className={element.covered ? 'SummaryElement SummaryElement--covered' : 'SummaryElement'}
          >
            {' • ' + element.text}
          </span>
        )
      }
    </div>
  )
}

export default Summary
