import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import './Hour.css'

const Hour = () => {
  const [date, setDate] = useState(new Date())
  const alarm = useSelector(state => state.clock.alarmHour)

  useEffect(() => {
    const timer = setInterval(() => setDate(new Date()), 1000)
    alert()
    return () => clearInterval(timer)
  })

  const alert = () => {
    const audio = new Audio('alarm.mp3') // eslint-disable-line
    audio.load()
    if (alarm === date.toLocaleTimeString()) {
      document.getElementById('Hour__text').classList.add('Hour__text--animated-ringing')
      setTimeout(
        () => { document.getElementById('Hour__text').classList.remove('Hour__text--animated-ringing') },
        5000)
      return (audio.play())
    }
  }

  const display = useSelector(state => state.clock.isDisplayed)

  return (
    <div className={display ? 'HourComponent' : 'HourComponent is-hidden'}>
      <p id='Hour__text'>  {date.toLocaleTimeString().slice(0, 5)}</p>
    </div>
  )
}

export default Hour
