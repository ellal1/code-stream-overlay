import React from 'react'
import './Question.css'
import { useSelector } from 'react-redux'

const Question = () => {
  const question = useSelector(state => state.question.queue[state.question.currentIndex])
  const display = useSelector(state => state.question.isDisplayed)
  return (
    <div className={display ? 'Question' : 'Question is-hidden'}>
      <span className='question__quotes question__quotes--open'>“</span>
      <div className='question__question-box question-box-container'>
        <div className='question-box__flag' />
        <div className='question-box__text-container'>
          <p className='text-container__text'>{question}</p>
        </div>
      </div>
      <span className='question__quotes question__quotes--close'>”</span>
    </div>
  )
}

export default Question
