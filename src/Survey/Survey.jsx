import React from 'react'
import './Survey.css'
import { useSelector } from 'react-redux'

const Option = ({ choice, percentage }) => (
  <div className='option'>
    <p className='option-text'>{choice}</p>
    <div className='progressBar' style={{ width: percentage + '%' }}>{percentage}%</div>
  </div>
)

const Survey = () => {
  const display = useSelector(state => state.survey.isDisplayed)
  const question = useSelector(state => state.survey.question)
  const options = useSelector(state => state.survey.options)
  const totalVotes =
    useSelector(state => state.survey.options)
      .reduce((total, option) => total + option.votes, 0)
  return (
    <div className={display ? 'Survey' : 'Survey is-hidden'}>
      <span className='Survey survey__box'>
        <p className='survey-text'>{question}</p>
      </span>
      <div className='option-box option-label'>
        {
          options.map(option =>
            <Option
              key={option.id}
              choice={option.choice}
              percentage={option.votes * 100 / totalVotes}
            />
          )
        }
      </div>
    </div>
  )
}

export default Survey
