import React, { useEffect, useState } from 'react'
import './Guideline.css'
import { useSelector } from 'react-redux'

const calculateTimeLeft = (timeOut) => {
  const currentTime = new Date()
  const endTime = new Date()
  endTime.setMinutes(timeOut.minutes)
  endTime.setHours(timeOut.hours)
  endTime.setSeconds(timeOut.seconds)
  const difference = endTime - currentTime
  let timeLeft = {}

  if (difference > 0) {
    timeLeft = {
      hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
      minutes: Math.floor((difference / 1000 / 60) % 60),
      seconds: Math.floor((difference / 1000) % 60)
    }
  }

  return timeLeft
}

const Guideline = () => {
  const timeOut = useSelector(state => state.guidelines.time)
  const guideline = useSelector(state => state.guidelines.guideline)
  const display = useSelector(state => state.guidelines.isDisplayed)

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(timeOut))

  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft(timeOut))
    }, 1000)
    // Clear timeout if the component is unmounted
    return () => clearTimeout(timer)
  })

  const timerComponents = []
  Object.keys(timeLeft).forEach((interval) => {
    if (!timeLeft[interval]) {
      return
    }
    timerComponents.push(
      <span className='Timer__text'>
        {timeLeft[interval].toString().padStart(2, '0')}{interval === Object.keys(timeLeft)[2] ? '' : ':'}
      </span>
    )
  })

  return (
    <div className={display ? 'Guidelines' : 'Guidelines is-hidden'}>
      <span className='OneGuideline'>
        <p className='OneGuideline__text'>CONSIGNES</p>
      </span>
      <span className='Instruction'>
        <p className='Instruction__text'>{guideline}</p>
      </span>
      <span className='Timer'>
        <i className='material-icons'>timer</i>
        <span>{timerComponents}</span>
      </span>
    </div>
  )
}

export default Guideline
