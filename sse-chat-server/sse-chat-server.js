const readline = require('readline')
const express = require('express')
const cors = require('cors')

const app = express() // Create express app
app.use(cors()) // Avoid CORS issues

// ****** SSE-SERVER ******
// Listens GET on localhost:3000/sse-chat-server
const port = 3000
app.listen(port, () => {
  console.log(`Listening at port ${port}`)
})
app.get('/sse-chat-server', (req, res) => {
  // Setting SSE header
  res.status(200).set({
    'connection': 'keep-alive',
    'cache-control': 'no-cache',
    'content-Type': 'text/event-stream'
  })
  console.log('Connection established on /sse-chat-server:3000') // Log in server console
  res.write('data: Connection to SSE chat server established\n\n') // Message sent to client
  // /!\ don't forget `data:` and `/n/n`, or message will be ignored

  // ****** READ USER INPUTS ******
  const r1 = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })
  // Get what user writes in the server console
  // recursive function, type exit to stop
  const getInputRec = (currentUser, res) => {
    r1.question(`${currentUser} ⌨️  sse-chat-server ▶️ `, (answer) => {
      // If user inputs '!!exit' : quit program
      if (answer === 'exit') {
        r1.close()
        res.end()
        console.log('Exited')
        process.exit(0)
      } else {
        // Else analyses input
        const processedInput = processInput(answer, currentUser)
        // and emits event to client
        app.emit('event:userInput', res, processedInput.data)

        // Recusivity
        getInputRec(processedInput.user, res)
      }
    })
  }
  // Start listening for inputs as admin (default user)
  getInputRec('admin', res)
})

// ****** EVENTS HANDLING ******
// events that send user input as sse message to client
app.on('event:userInput', (res, input) => {
  res.write(`data: ${input}\n\n`)
  console.log(`--> ${input}`)
})

// Manage different inputs and commands
const processInput = (input, user) => {
  switch (input) {
    /* default user is 'viewer', but can be changed
       by typing 'setuser admin'. Switch back to
       'viewer by typing 'setuser viewer' */
    case 'setuser admin':
      console.log('>> User set to admin')
      return {
        user: 'admin',
        data: null
      }
    case 'setuser viewer':
      console.log('>> User set to viewer')
      return {
        user: 'viewer',
        data: null
      }

      // Default is considered as a chat message
      /*
      Chat messages are JSON :
      {
        'user': [admin or viewer]
        'content': [what server user typed]
      }
    */
    default:
      return {
        user: user,
        data: '{"user": "' + user + '", "content": "' + input + '"}'
      }
  }
}
