# Code Stream Overlay

by Laurine Capdeville, Léa Rostoker, Éloïse Tassin, Luc Pinguet and Alaric Rougnon-Glasson (IMAC 2022).

## What is it ?

This a second year of IMAC Engineering School (ESIPE, Gustave Eiffel University, France) web development project.
The goal is to create a bunch of components to use in livestream overlays (in OBS Studio for example).

## How to use

Install all node.js modules dependencies using `npm install`.
Then, you can run the project using `npm start` or `npm run start` (or `snowpack start`, as we are using the Snowpack bundler).

### To use the chat server

Install all node.js modules dependencies using `npm install` in the `sse-chat-server` folder.
Then, run the the server using `node sse-chat-server.js`
The chat server will appear in the terminal and you will be able to use commands from the **Commands** section of this document.

## File structure

**Components** are located in `src/`. Each one has a directory containing 'component.jsx' and 'component.css' files.
**Public files** (`index.html`, `index.js` and `global.css` that contains and call everything else) are located in the `/public` directory.
**Chat Server Simulation** is located in `sse-chat-server` which will allow you to call different commands and so fully use our components.

## Commands

`setuser` : allows you to change from **admin** to **viewer** profile to simulate messages sent from the streamer or any other user watching the live stream.

### For the **Clock** :

`!!clock-toggle` : toggles its visibility (usable only by `admin`).

`!!clock-set` : sets a time for the end of the class (usable only by `admin`).

### For the **PresenceGrid** :

`!!call-toggle` : toggles its visibility (usable only by `admin`).

`!!call-reset` : resets the studentList (usable only by `admin`).

`!!call-present` : changes the value to `true` of the `present` attribute of a student in the studentList (usable only by `viewer`).

### For the **Question** :

`!!question-toggle` : toggles its visibility (usable only by `admin`).

`!!question-next` : display the next question in the queue (usable only by `admin`).

`!!question-previous` : display the previous question in the queue (usable only by `admin`).

`!!question-new` : add a question to the queue (usable only by `viewer`). *Exemple : `!!summary-new Where's my friend ?`*

### For the **Guidelines** :

`!!guideline-toggle` : toggles its visibility (usable only by `admin`).

`!!guideline-set` : sets a guideline with an endtime to create a countdown (usable only by `admin`). *Exemple : `!!guideline-set 15:45:30 Finish Exercice 3`*

### For the **Summary** :

`!!summary-toggle` : toggles its visibility (usable only by `admin`).

`!!summary-covered` : sets an element of the summary as **covered** (usable only by `admin`).

`!!summary-new` : adds a new element in the summary (usable only by `admin`). *Exemple : `!!summary-new Title 3`*

### For the **Survey** :

`!!survey-toggle` : toggles its visibility (usable only by `admin`).

`!!survey-new` : creates a new survey(usable only by `admin`). *Exemple : `!!survey-new question // option1 // option2 // ...`* 

`!!survey-vote` : votes for one of the survey options (usable only by `viewer`). *Exemple : `!!survey-vote 2`*